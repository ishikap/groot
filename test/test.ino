/*

*/
#include <cmath>
#include <algorithm>


// Motor Digital Inputs 
#define IN1_RIGHT 12
#define IN2_RIGHT 11
#define IN3_LEFT  10
#define IN4_LEFT  9

// Enable for motor PWM 
#define EN_RIGHT 14
#define EN_LEFT  15

// Pin for reading photoresistor
#define PHOTO_FRONT  17
#define PHOTO_BACK   18
#define PHOTO_CENTER 19
#define PHOTO_RIGHT  21
#define PHOTO_LEFT   22


void setup() {
    
    // Digital write pins for motor enable
    pinMode(IN1_RIGHT, OUTPUT);  
    pinMode(IN2_RIGHT, OUTPUT);  
    pinMode(IN3_LEFT, OUTPUT);  
    pinMode(IN4_LEFT, OUTPUT); 

    // Analog PWM pins for motor speed
    pinMode(EN_RIGHT, OUTPUT); 
    pinMode(EN_LEFT, OUTPUT); 
    
    // Analog Read pins for photoresistors
    pinMode(PHOTO_FRONT, INPUT);
    pinMode(PHOTO_BACK, INPUT);
    pinMode(PHOTO_CENTER, INPUT);
    pinMode(PHOTO_RIGHT, INPUT);
    pinMode(PHOTO_LEFT, INPUT);
    Serial.begin(9600);
}

void loop() {
    int front = analogRead(PHOTO_FRONT);
    int back = analogRead(PHOTO_BACK);
    int center = analogRead(PHOTO_CENTER);
    int right = analogRead(PHOTO_RIGHT);
    int left = analogRead(PHOTO_LEFT);

    front = map(front, 0, 1023, 0, 200);
    back = map(back, 0, 1023, 0, 200);
    center = map(center, 0, 1023, 0, 200);
    right = map(right, 0, 1023, 0, 200);
    left = map(left, 0, 1023, 0, 200);
   
    Serial.print("front");
    Serial.print(front);
    Serial.print("\n"); 
    Serial.print("back");
    Serial.print(back);
    Serial.print("\n"); 
    Serial.print("left");
    Serial.print(left);
    Serial.print("\n"); 
    Serial.print("right");
    Serial.print(right);
    Serial.print("\n"); 
    Serial.print("center");
    Serial.print(center);
    Serial.print("\n"); 
    int threshold = 15;
    int in1_right = LOW;
    int in2_right = LOW;
    int in3_left = LOW;
    int in4_left = LOW;
    int en_left = 0;
    int en_right = 0;
    delay(50);
    // Don't Move
    if(center>(front+threshold) && center>(back+threshold) 
	&& center>(left+threshold) && center>(right+threshold)) {
	
	in1_right = LOW;
	in2_right = LOW;
	in3_left = LOW;
	in4_left = LOW;
	en_left = 0;
	en_right = 0;
        Serial.print("CENTER\n");

    }
    // Move straight forward
    else if(front>(back+threshold)&& abs(right-left)<threshold) {
	in1_right = HIGH;
	in2_right = LOW;
	in3_left = LOW;
	in4_left = HIGH;
	en_left = max(front,150);
	en_right = max(front,150);
        Serial.print("STRAIGHT FOWARD\n");
    }
    // Move straight backward
    else if(back>(front+threshold) && abs(right-left)<threshold) {
	in1_right = LOW;
	in2_right = HIGH;
	in3_left = HIGH;
	in4_left = LOW;
	en_left = max(back,150);
	en_right = max(back,150);
       	Serial.print("STRAIGHT BACkWARD\n");
    }
    // Turn Right Backwards, Left wheels go faster
    else if(right>(left+threshold) && back>(front+threshold)) {
	in1_right = LOW;
	in2_right = HIGH;
	in3_left = HIGH;
	in4_left = LOW;
	en_left = max(right,150);
	en_right = max(en_left-25,0);
	Serial.print("RIGHT TURN BACK\n");
    }
    // Turn Left Backwards, Right wheels go faster
    else if(left>(right+threshold) && back>(front+threshold)) {
	in1_right = LOW;
	in2_right = HIGH;
	in3_left = HIGH;
	in4_left = LOW;
	en_right = max(left,150);
	en_left = max(en_right-25,0);
	Serial.print("LEFT TURN BACK\n");
    }
    // Turn Right Forward, Left wheels go faster
    else if(right>(left+threshold)) {
	in1_right = HIGH;
	in2_right = LOW;
	in3_left = LOW;
	in4_left = HIGH;
	en_left = max(right,150);
	en_right = max(en_left-25,0);
	Serial.print("RIGHT TURN FOR\n");
    }
    // Turn Left Forwards, Right wheels go faster
    else if(left>(right+threshold)) {
	in1_right = HIGH;
	in2_right = LOW;
	in3_left = LOW;
	in4_left = HIGH;
	en_right = max(left,150);
	en_left = max(en_right-25,0);
	Serial.print("LEFT TURN FOR\n");
    }
    // Don't Move
    else {
	in1_right = HIGH;
	in2_right = LOW;
	in3_left = LOW;
	in4_left = HIGH;
	en_right = 0;
	en_left = 0;
    }
        digitalWrite(IN1_RIGHT, in1_right);
	digitalWrite(IN2_RIGHT, in2_right);
	digitalWrite(IN3_LEFT, in3_left);
	digitalWrite(IN4_LEFT, in4_left);

	analogWrite(EN_LEFT, en_left);	
	analogWrite(EN_RIGHT, en_right);
		
    //delay(100); // Just adding this, just in case

}
